import {Component, NgZone, OnInit} from '@angular/core';
import { ModalOptions } from '@ionic/core';
import {ActionSheetController, AlertController, LoadingController, ModalController, NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {MediaService} from '../../core/services/media.service';
import {ComplaintProtectionMeasuresPage} from '../complaint-protection-measures/complaint-protection-measures.page';
import {ApiService} from '../../core/services/api.service';
import {UtilsService} from '../../core/services/utils.service';
import {defaultBase64} from '../../core/constants/media';
import {ImageBase64ModalPage} from '../image-base64-modal/image-base64-modal.page';

const terms = `
  En virtud a lo dispuesto en el articulo 7 del Decreto Legislativo Nº 1327, me
  comprometo a permanecer a disposición de la entidad, a fin de brindar las
  aclaraciones que hagan falta o proveer mayor información a la entidad, sobre
  los presuntos hechos que motivaron la denuncia presentada. Asimismo,
  también me comprometo a cumplir con las obligaciones establecida en el
  Decreto Legislativo Nº 1327 y su reglamento aprobado por el Decreto Supremo
  Nº 010-2017-JUS. Asimismo, declaro que toda la información alcanzada se
  ajusta a la verdad y puede ser comprobada administrativamente de acuerdo a
  las atribuciones legales señaladas en la normativa aplicable.
`;

@Component({
  selector: 'app-complaint-step4',
  templateUrl: './complaint-step4.page.html',
  styleUrls: ['./complaint-step4.page.scss'],
})
export class ComplaintStep4Page implements OnInit {
  complaintType;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private media: MediaService,
    private modalCtrl: ModalController,
    public api: ApiService,
    private loadingCtrl: LoadingController,
    private utils: UtilsService,
    private ngZone: NgZone,
  ) { }

  ngOnInit() {
    this.complaintType = this.route.snapshot.paramMap.get('complaintType');
  }

  back() {
    this.navCtrl.navigateBack(['/complaint', this.complaintType, 'step3']);
  }

  backAll() {
    this.navCtrl.navigateBack(['/home']);
  }

  async addFileOptions() {
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          icon: 'camera',
          text: 'Cámara',
          handler: () => this.openCamera()
        },
        {
          icon: 'images',
          text: 'Galería',
          handler: () => this.openGallery()
        },
        /*
        {
          icon: 'folder',
          text: 'Archivos',
          handler: () => this.openChooser()
        }
        */
      ]
    });
    await actionSheet.present();
  }

  async confirmProtection() {
    const alert = await this.alertCtrl.create({
      message: 'Desea solicitar medidas de protección?',
      buttons: [
        {
          text: 'Si',
          handler: () => { this.addProtection(); }
        },
        {
          text: 'No',
          handler: () => { this.confirmTerms(); }
        }
      ]
    });
    await alert.present();
  }


  async addProtection() {
    const opts: ModalOptions<any> = {
      component: ComplaintProtectionMeasuresPage,
    };
    const modal = await this.modalCtrl.create(opts);
    modal.present();
    modal.onDidDismiss()
      .then(({data}) => {
        if (data) { this.confirmTerms(); }
      });
  }

  async confirmTerms() {
    const alert = await this.alertCtrl.create({
      cssClass: 'ion-text-justify alert-message-lg',
      message: terms,
      buttons: [
        {
          text: 'No Acepto',
          handler: () => {}
        },
        {
          text: 'Acepto',
          handler: () => { this.save(); }
        }
      ]
    });
    await alert.present();
  }

  async save() {
    const loading = await this.loadingCtrl.create({});
    loading.present();
    this.api.saveAll()
      .subscribe(
        async res => {
          console.log(res)
          await loading.dismiss();
        },
        async err => {
          await loading.dismiss();
          console.log(err);
        },
        () => {
          console.log('completed');
          this.backAll();
          this.basicAler('La Denuncia a sido guardada exitosamente');
        }
      );
  }

  async basicAler(message) {
    const alert = await this.alertCtrl.create({ message });
    await alert.present();
  }

  private openCamera() {
    this.media.mediaCamera()
      .then(base64string => this.addFile(base64string))
      .catch(err => console.log(err));
  }

  private openGallery() {
    this.media.mediaGallery()
      .then(base64string => this.addFile(base64string))
      .catch(err => console.log(err));
  }

  /*
  private openChooser() {
    this.media.mediaChooser()
      .then(uri => this.addFile(uri))
      .catch(err => console.log(err));
  }

  addFile(imageURI: string) {
    if (!this.utils.isHybrid()) {
      const path = imageURI.split('/');
      const fileName = path[path.length - 1];
      this.ngZone.run(() => {
        this.api.files.push({
          name: fileName,
          base64: defaultBase64
        });
      });
      return;
    }
    this.media.mediaUpload(imageURI)
      .then(base64String => {
        const path = imageURI.split('/');
        const fileName = path[path.length - 1];
        this.ngZone.run(() => {
          this.api.files.push({
            name: fileName,
            base64: base64String
          });
        });
      })
      .catch(err => {console.log(err); });
  }

  */

  addFile(base64String: string) {
    const fileName = this.media.generateFileName();
    this.ngZone.run(() => {
      this.api.files.push({
        name: fileName,
        base64: base64String
      });
    });
  }

  removeFile(index) {
    this.api.files.splice(index, 1);
  }

  async openImageBase64(base64Image) {
    const opts: ModalOptions<any> = {
      component: ImageBase64ModalPage,
      componentProps: {base64Image}
    };
    const modal = await this.modalCtrl.create(opts);
    modal.present();
  }
}
