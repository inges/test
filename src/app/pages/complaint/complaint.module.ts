import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComplaintPage } from './complaint.page';
import {ComponentsModule} from '../../shared/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ComplaintPage
  },
  { path: ':complaintType/step0', loadChildren: '../complaint-step0/complaint-step0.module#ComplaintStep0PageModule' },
  { path: ':complaintType/step1', loadChildren: '../complaint-step1/complaint-step1.module#ComplaintStep1PageModule' },
  { path: ':complaintType/step2', loadChildren: '../complaint-step2/complaint-step2.module#ComplaintStep2PageModule' },
  { path: ':complaintType/step3', loadChildren: '../complaint-step3/complaint-step3.module#ComplaintStep3PageModule' },
  { path: ':complaintType/step4', loadChildren: '../complaint-step4/complaint-step4.module#ComplaintStep4PageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [ComplaintPage]
})
export class ComplaintPageModule {}
