import {Component, NgZone, OnInit} from '@angular/core';
import { ModalOptions } from '@ionic/core';
import {ModalController, NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';
import {ComplaintOfficesModalPage} from '../complaint-offices-modal/complaint-offices-modal.page';
import {IOffice} from '../../core/models/office';


@Component({
  selector: 'app-complaint-step2',
  templateUrl: './complaint-step2.page.html',
  styleUrls: ['./complaint-step2.page.scss'],
})
export class ComplaintStep2Page implements OnInit {
  complaintType;
  offices: IOffice[] = [];
  form: FormGroup;
  formOffice: FormGroup;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public api: ApiService,
    public modalCtrl: ModalController,
    public ngZone: NgZone,
  ) {
    this.formOffice = this.formBuilder.group({
      id: ['', Validators.required],
      text: ['', Validators.required],
    });
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      position: ['', Validators.required],
      office: this.formOffice
    });
  }

  ngOnInit() {
    this.complaintType = this.route.snapshot.paramMap.get('complaintType');
  }

  next() {
    this.navCtrl.navigateForward(['/complaint', this.complaintType, 'step3']);
  }

  back() {
    this.navCtrl.navigateBack(['/complaint', this.complaintType, 'step1']);
  }

  addFunctionary(){
    console.log('entre');
    console.log(this.form.value);
    this.ngZone.run(() => {
      this.api.functionaries.push(this.form.value);
      this.form.reset();
    });
  }

  removeFunctionary(index) {
    this.api.functionaries.splice(index, 1);
  }

  async searchOffices() {
    const office: IOffice = {
      codigo: this.formOffice.value.id,
      NOMBRE: this.formOffice.value.text,
    };
    const opts: ModalOptions<any> = {
      component: ComplaintOfficesModalPage,
      componentProps: {office, offices: this.offices},
      backdropDismiss: false
    };
    const modal = await this.modalCtrl.create(opts);
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      console.log(data);
      this.offices = data.offices;
      if (data.office) {
        this.formOffice.patchValue({
          id: data.office.codigo,
          text: data.office.NOMBRE,
        });
      }
    }
  }
}
