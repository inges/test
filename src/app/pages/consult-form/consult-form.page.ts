import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-consult-form',
  templateUrl: './consult-form.page.html',
  styleUrls: ['./consult-form.page.scss'],
})
export class ConsultFormPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  next() {
      this.navCtrl.navigateForward('/consult/step2');
  }

  back() {
      this.navCtrl.navigateBack('/consult');
  }

}
