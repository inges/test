import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-consult-detail',
  templateUrl: './consult-detail.page.html',
  styleUrls: ['./consult-detail.page.scss'],
})
export class ConsultDetailPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  back() {
      this.navCtrl.navigateBack('/consult/step2');
  }

}
