import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-image-base64-modal',
  templateUrl: './image-base64-modal.page.html',
  styleUrls: ['./image-base64-modal.page.scss'],
})
export class ImageBase64ModalPage implements OnInit {
  @Input() base64Image: string;

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  close() {
    this.modalCtrl.dismiss();
  }

}
