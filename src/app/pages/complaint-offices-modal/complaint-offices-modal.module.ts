import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComplaintOfficesModalPage } from './complaint-offices-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  entryComponents: [ComplaintOfficesModalPage],
  declarations: [ComplaintOfficesModalPage]
})
export class ComplaintOfficesModalPageModule {}
