import { Component, OnInit } from '@angular/core';
import {LoadingController, NavController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';

@Component({
  selector: 'app-consult',
  templateUrl: './consult.page.html',
  styleUrls: ['./consult.page.scss'],
})
export class ConsultPage implements OnInit {
  formGroup: FormGroup;

  constructor(
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private api: ApiService,
    private loadingCtrl: LoadingController
  ) {
    this.formGroup = this.formBuilder.group({
      registryNumber: ['', Validators.required],
      claveNumber: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  async search() {
    const loading = await this.loadingCtrl.create({});
    loading.present();
    this.api.getProcedure(this.formGroup.value.registryNumber)
      .subscribe(
        async res => {
          console.log(res);
          await loading.dismiss();
          await this.next();
        },
        async err => {
          await loading.dismiss();
          console.log(err);
        }
      );
  }

  async next() {
    await this.navCtrl.navigateForward('/consult/step1');
  }

  back() {
    this.navCtrl.navigateBack('/home');
  }

}
