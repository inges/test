import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConsultPage } from './consult.page';
import {ComponentsModule} from '../../shared/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: ConsultPage
  },
  { path: 'step1', loadChildren: '../consult-form/consult-form.module#ConsultFormPageModule' },
  { path: 'step2', loadChildren: '../consult-result/consult-result.module#ConsultResultPageModule' },
  { path: 'step3', loadChildren: '../consult-detail/consult-detail.module#ConsultDetailPageModule' },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [ConsultPage]
})
export class ConsultPageModule {}
