import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {DATETIME} from '../../core/utils/constants';
import {UbigeoService} from '../../core/services/ubigeo.service';
import * as Ubigeo from '../../core/models/ubigeo';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';

@Component({
  selector: 'app-complaint-step1',
  templateUrl: './complaint-step1.page.html',
  styleUrls: ['./complaint-step1.page.scss'],
})
export class ComplaintStep1Page implements OnInit {
  complaintType;
  DATETIME = DATETIME;
  departments: Ubigeo.Department[];
  provinces: Ubigeo.Province[];
  districts: Ubigeo.District[];
  form: FormGroup;
  departmentField: FormControl;
  provinceField: FormControl;
  districtField: FormControl;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private ubigeo: UbigeoService,
    private fb: FormBuilder,
    private api: ApiService,
  ) {
    this.departmentField = new FormControl('');
    this.provinceField = new FormControl('');
    this.districtField = new FormControl('');
    this.form = this.fb.group({
      dateFact: ['', Validators.required],
      committedEntities: '',
      department: this.departmentField,
      province: this.provinceField,
      district: this.districtField
    });

    this.departmentField.valueChanges.subscribe(value => {
      this.provinceField.patchValue('');
    });
    this.provinceField.valueChanges.subscribe(value => {
      this.districtField.patchValue('');
    });
  }

  async ngOnInit() {
    this.complaintType = this.route.snapshot.paramMap.get('complaintType');
    await this.loadUbigeo();

    this.form.patchValue({
      dateFact: this.api.fact.dateFact,
      committedEntities: this.api.fact.committedEntities,
      department: this.api.fact.department,
      province: this.api.fact.province,
      district: this.api.fact.district
    });

    this.form.valueChanges.subscribe((values) => {
      this.api.fact.dateFact = values.dateFact;
      this.api.fact.committedEntities = values.committedEntities;
      this.api.fact.department = values.department;
      this.api.fact.province = values.province;
      this.api.fact.district = values.district;
    });
  }

  async loadUbigeo() {
    await this.ubigeo.getData()
      .toPromise()
      .then((res: any) => {
          this.departments = res.departments;
          this.provinces = res.provinces;
          this.districts = res.districts;
        },
        err => console.error(err),
      );
  }

  next() {
    this.navCtrl.navigateForward(['/complaint', this.complaintType, 'step2']);
  }

  back() {
    this.navCtrl.navigateBack(['/complaint', this.complaintType, 'step0']);
  }

  get provincesList() {
    const departmentId =  this.form.value.department;
    if (departmentId) {
      return this.provinces[departmentId];
    }
    return [];
  }

  get districtsList() {
    const provinceId =  this.form.value.province;
    if (provinceId) {
      return this.districts[provinceId];
    }
    return [];
  }
}
