import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';

@Component({
  selector: 'app-complaint-step3',
  templateUrl: './complaint-step3.page.html',
  styleUrls: ['./complaint-step3.page.scss'],
})
export class ComplaintStep3Page implements OnInit {
  complaintType;
  form;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private api: ApiService,
  ) {
    this.form = this.fb.group({
      detailsFact: ['', Validators.required],
      sourceInformation: ['', Validators.required],
      supportingEvidence: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.complaintType = this.route.snapshot.paramMap.get('complaintType');

    this.form.patchValue({
      detailsFact: this.api.fact.detailsFact,
      sourceInformation: this.api.fact.sourceInformation,
      supportingEvidence: this.api.fact.supportingEvidence,
    });

    this.form.valueChanges.subscribe((values) => {
     
      this.api.fact.detailsFact = values.detailsFact;
      this.api.fact.sourceInformation = values.sourceInformation;
      this.api.fact.supportingEvidence = values.supportingEvidence;
    });
  }

  next() {
    this.navCtrl.navigateForward(['/complaint', this.complaintType, 'step4']);
  }

  back() {
    this.navCtrl.navigateBack(['/complaint', this.complaintType, 'step2']);
  }
}
