import {Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ApiService} from '../../core/services/api.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-complaint-protection-measures',
  templateUrl: './complaint-protection-measures.page.html',
  styleUrls: ['./complaint-protection-measures.page.scss'],
})
export class ComplaintProtectionMeasuresPage implements OnInit {
  form: FormGroup;


componentes: Componente[]=[
  {
    icon: 'american-foobtal',
    name: 'Renovación de relación contractual, convenio, etc.',
    value: '4'
  },  

  {icon: 'american-foobtal',
  name: 'Licencia sin goce de remuneración',
  value: '5'},  

  {icon: 'american-foobtal',
  name: 'Exoneración de obligación de asistir al trabajo',
  value: '6'},  

  {icon: 'american-foobtal',
  name: 'Otras medidas de protección',
  value: '7'},  
]

  constructor(
    private modalCtrl: ModalController,
    private api: ApiService,
    private fb: FormBuilder,
    
  ) {
    this.form = this.fb.group({
      tipoMedida: '', 
      otherIntegrity: '',
    });
  }

  ngOnInit() {
    this.api.complaint.isIntegrity = 1;

    this.form.patchValue({
      tipoMedida : this.api.medida.tipoMedida,
      otherIntegrity: this.api.medida.othersMedida,
     
    });

    this.form.valueChanges.subscribe(values => {
      this.api.medida.tipoMedida = values.tipoMedida;
      this.api.medida.othersMedida = values.otherIntegrity;
    });
   
  }

  dismiss(confirm: boolean) {
    this.modalCtrl.dismiss(confirm);
  }

}

interface Componente{
icon: string;
name: string;
value: string;
} 



