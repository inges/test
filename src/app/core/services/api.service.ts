import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ComplaintType, DocumentType, PersonType} from '../utils/constants';
import {defaultVal} from '../utils/functions';
import {forkJoin, Observable} from 'rxjs';
import {IReniecResponse} from '../models/reniec';
import {ISunatResponse} from '../models/sunat';
import {AlertController} from '@ionic/angular';
import {IOffice} from '../models/office';
import {UtilsService} from './utils.service';
import {Validators} from '@angular/forms';

/*
const defaultComplaint = {
  pFLG_ANONIMO: 0, // <---
  pFLG_INTEGRIDAD: 0,
  pFLG_SECRETARIA: 0,
  pTIPO_PERSONA: 0, // <---
  pTIPO_DOCUMENTO: '', // <---
  pNUM_DOCUMENTO: '', // <---
  PAPELLIDO_PATERNO: '',
  pAPELLIDO_MATERNO: '',
  pNOMBRES: '', // <---
  pDOMICILIO: '', // <---
  pTELEFONO: '', // <---
  pCORREO: '', // <---
  pTIPO_DOC_REP: 1,
  pNUM_DOC_REP: 1,
  pREPRESENTANTE: 'GEO',
  pEXTENSIVAS: 'NELL',
  pOTRAS_MEDIDAS: 1,
  pUSUARIO: 'GIORGIO', // Todo: unknown
  pIP: 54252, // Todo: unknown
  pCANALCOMUNICACION: 1, // Todo: unknown
  pID_DENUNCIA: 22, // Todo: unknown
};

const defaultFact = {
  pID_DENUNCIA: 85,
  pHECHO: 'SE ACABO EL CODIGO EEE',
  pUBIGEO: 245,
  pFUENTE_INFOR: 'INTERNET',
  pPRUEBAS_SUST: 'SIHAYVAROAS',
  pENTIDADES_COMPRENDIDAS: 'MINJUSPRODUCE',
  pUSUARIO: 'GEO',
  pIP: 735,
  pID_HECHO: 735
};
*/
class Complaint {
  documentType = '';
  documentNumber = '';
  motherLastName = '';
  fatherLastName = '';
  fullName = '';
  address = '';
  phone = '';
  email = '';

  isAnonymous = 0;
  isIntegrity = 0;
  otherIntegrity = '';
  personType = PersonType.ANONYMOUS;
}

class Fact {
  dateFact = '';
  committedEntities = '';
  department = '';
  province = '';
  district = '';
  detailsFact = '';
  sourceInformation = '';
  supportingEvidence = '';
}

class Functionary {
  name = '';
  position = '';
  office = {
    id: '',
    text: ''
  };
}

class File {
  name = '';
  base64 = '';
}

class Medida {
  tipoMedida = '';
  othersMedida = '';
  
}

const BASE_URL = 'http://190.116.91.89/API_IMG';
const USER = 'APP';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public complaint = new Complaint() ;
  public fact = new Fact();
  public functionaries: Functionary[] = [];
  public files: File[] = [];
  public medida= new Medida();

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) {
  }

  initialize(complaintType) {
    this.complaint = new Complaint();
    this.fact = new Fact();
    this.functionaries = [];
    this.files = [];

    switch (complaintType) {
      case ComplaintType.anonymous:
        this.complaint.isAnonymous = 1;
        break;
      case ComplaintType.natural:
        this.complaint.documentType = DocumentType.DNI;
        this.complaint.personType = PersonType.NATURAL;
        break;
      case ComplaintType.legal:
        this.complaint.documentType = DocumentType.RUC;
        this.complaint.personType = PersonType.LEGAL;
        break;
    }
  }

  saveMedida(complaintId) {
    const tipoMedida = defaultVal(this.medida.tipoMedida, 'null');
    const othersMedida = defaultVal(this.medida.othersMedida, 'null'); 
    const uuid = this.utils.getDeviceUUID();
    return this.http.get(`${BASE_URL}/SerMedidas.svc/Ser_medida/${tipoMedida}/${complaintId}/${othersMedida}` +
      `/${USER}/${uuid}`);
  }

  saveComplaint() {
    const documentType =  defaultVal(this.complaint.documentType, 0);
    const documentNumber = defaultVal(this.complaint.documentNumber, '000000');
    const motherLastName = defaultVal(this.complaint.motherLastName, 'null');
    const fatherLastName = defaultVal(this.complaint.motherLastName, 'null');
    const fullName = defaultVal(this.complaint.fullName , 'null');
    const address = defaultVal(this.complaint.address, 'null');
    const phone = defaultVal(this.complaint.phone, 'null');
    const email = defaultVal(this.complaint.email, 'null');
    const isAnonymous = defaultVal(this.complaint.isAnonymous, 0);
    const isIntegrity = defaultVal(this.complaint.isIntegrity, 0);
    const otherIntegrity = defaultVal(this.complaint.otherIntegrity, 'null');
    const personType = defaultVal(this.complaint.personType, PersonType.ANONYMOUS);
    const uuid = this.utils.getDeviceUUID();

    return this.http.get(`${BASE_URL}/SerDenuncia.svc/Post_Denuncia/` +
    `${isAnonymous}/${isIntegrity}/1/${personType}/` +
    `${documentType}/${documentNumber}/${motherLastName}/` +
    `${fatherLastName}/${fullName}/${address}/${phone}/` +
    `${email}/1/1/GEO/NELL/${otherIntegrity}/` +
    `${USER}/${uuid}/1/0`);


    // http://200.60.112.217/API_IMG/SerDenuncia.svc/Post_Denuncia/{pFLG_ANONIMO}/
    // {pFLG_INTEGRIDAD}/{pFLG_SECRETARIA}/{pTIPO_PERSONA}/{pTIPO_DOCUMENTO}/{pNUM_DOCUMENTO}/
    // {PAPELLIDO_PATERNO}/{pAPELLIDO_MATERNO}/{pNOMBRES}/{pDOMICILIO}/{pTELEFONO}/{pCORREO}/
    // {pTIPO_DOC_REP}/{pNUM_DOC_REP}/{pREPRESENTANTE}/{pEXTENSIVAS}/{pOTRAS_MEDIDAS}/{pUSUARIO}/
    // {pIP}/{pCANALCOMUNICACION}/{pID_DENUNCIA

    // http://200.60.112.217/API_IMG/SerDenuncia.svc/Post_Denuncia/1/1/1/1/1/45796435/FIGUEROA/
    // CABRERA/GEORGE/JICAMARCA/52462/GEOXUNIX@GMAIL.COM/1/1/GEO/NELL/1/GIORGIO/54252/1/22
  }

  saveFact(complaintId) {
    const detailsFact = defaultVal(this.fact.detailsFact, 'null');
    const sourceInformation = defaultVal(this.fact.sourceInformation, 'null');
    const committedEntities = defaultVal(this.fact.committedEntities, 'null');
    const supportingEvidence = defaultVal(this.fact.supportingEvidence, 'null');
    const district = defaultVal(this.fact.district, 'null'); // ubigeo only district code
    const uuid = this.utils.getDeviceUUID();

    return this.http.get(`${BASE_URL}/Serhecho.svc/Ser_hecho/${complaintId}/${detailsFact}/${district}` +
      `/${sourceInformation}/${supportingEvidence}/${committedEntities}/${USER}/${uuid}/0`);

      // Serhecho.svc/Ser_hecho/85/SE ACABO EL CODIGO EEE/245/INTERNET/SIHAYVAROAS/MINJUSPRODUCE/GEO/735/735
  }

  saveFunctionary(complaintId, factId, functionary: Functionary) {
    const functionaryName = defaultVal(functionary.name, 'null');
    const functionaryPosition = defaultVal(functionary.position, 'null');
    const functionaryOfficeId = defaultVal(functionary.office.id, 'null');

    const uuid = this.utils.getDeviceUUID();

    return this.http.get(`${BASE_URL}/Serfuncionario.svc/Post_Funcionario/${factId}/${complaintId}` +
      `/${functionaryName}/${functionaryPosition}/${functionaryOfficeId}/${USER}/${uuid}`);

    /*
      web service insertar funcionario de la denuncia:
      parametros de entrada:
      pID_HECHO       INT,
      pID_DENUNCIA    NUMERIC,
      pFUNCIONARIO    VARCHAR2,
      pCARGO          VARCHAR2,
      pID_OFICINA     VARCHAR2,
      pUSUARIO        VARCHAR2,
      pIP             VARCHAR2
    */
  }

  /*
  saveFile(complaintId, file: File) {
    const fileName = defaultVal(file.name, 'null');

    const uuid = this.utils.getDeviceUUID();

    return this.http.get(`${BASE_URL}/SerArchivo.svc/Post_Archivo/${fileName}/dato4/${complaintId}/${USER}/` +
      `${uuid}/tbregsbrrtser5635625445634/1`);
    */
    /*
      Insertar archivos -- a la Denuncia:
      parametros entrada:
      pNOMBRE VARCHAR2,
      pARCHIVO_GENERADO VARCHAR2,
      pID_DENUNCIA NUMBER,
      pUSUARIO VARCHAR2,
      pIP VARCHAR2,
      pID_LASERFICHE BLOB,
      pTipoArchivo VARCHAR2
    */
  // }


  saveFile(complaintId, file: File) {
    const uuid = this.utils.getDeviceUUID();

    const datainfo: FormData = new FormData();
    datainfo.append( 'nombre', file.name);
    datainfo.append( 'archivo_generado', 'app');
    datainfo.append( 'id_denuncia', complaintId);
    datainfo.append( 'usu_ingreso', 'App');
    datainfo.append( 'ip_ingreso', uuid);
    datainfo.append( 'laserfiche', encodeURIComponent(file.base64));
    datainfo.append( 'tipo_archivo', '1');

    return this.http.post(`${BASE_URL}/SerImagen.svc/Upload`, datainfo);
  }

  saveAll() {
    return new Observable(subscriber => {
      this.saveComplaint()
        .subscribe(
          (complaintRes: any) => {
            console.log(complaintRes);
            subscriber.next(1);
            const complaintId = complaintRes.ID_DENUNCIA;
            this.saveFact(complaintId)
              .subscribe(
                (factRes: any) => {
                  console.log(factRes);
                  subscriber.next(2);
                  const factId = factRes.Post_HechoResult.ID_HECHO;
                  const functionaries = this.functionaries.map(functionary => this.saveFunctionary(complaintId, factId, functionary));
                  // const files = [];
                  const files = this.files.map(file => this.saveFile(complaintId, file));
                  const med = this.saveMedida(complaintId); 
                  const all = functionaries.concat(files).push(med);
                  console.log('terminaras0');
                  forkJoin(all)
                    .subscribe(
                      (allRes: any) => {
                        console.log(allRes);
                        subscriber.next(4);
                        subscriber.complete();
                      },
                      err => { subscriber.error(err); }
                    );
                },
                err => { subscriber.error(err); }
              );
          },
          err => { subscriber.error(err); }
        );
    });
  }

  getSunatInfo(ruc: number): Observable<ISunatResponse> {
      return this.http.get<ISunatResponse>(`${BASE_URL}/Servicepide.svc/Post_Sunat/${ruc}`);
  }

  getReniecInfo(dni: number): Observable<IReniecResponse> {
      return this.http.get<IReniecResponse>(`${BASE_URL}/Servicepide.svc/Post_Reniec/${dni}`);
  }

  getOffices(): Observable<IOffice[]> {
    return this.http.get<IOffice[]>(`${BASE_URL}/Oficina.svc/Get_oficinas`);
  }

  getProcedure(registryNumber): Observable<IOffice[]> {

    return this.http.get<IOffice[]>(`http://172.25.2.100/SharePoint/srvTramite.svc/Get_Expediente/${registryNumber}`);

  }

}
