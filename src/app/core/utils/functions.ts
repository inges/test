
export const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};


export const defaultVal = (val, newVal) => {
  return val ? val : newVal;
};

export const cleanTick = (text): string => {
  text = text.toLowerCase();
  text = text.replace(/á/gi, 'a');
  text = text.replace(/é/gi, 'e');
  text = text.replace(/í/gi, 'i');
  text = text.replace(/ó/gi, 'o');
  text = text.replace(/ú/gi, 'u');
  return text;
};

export const uuidv4 = (): string => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
