export class IExpediente{
    ID_EXPEDIENTE  : string;
    ASUNTO_ADICIONAL  : string;
    REMITENTE  : string;
    CODIGO_DOCUMENTO  : string;
    FECHA_RECEPCION  : string;
    ASUNTO  : string;
    OBSERVACION  : string;
    CONGRESO  : string;
    ID_EXPEDIENTE_ANEXADO  : string;
    ID_EXPEDIENTE_REFERENCIADO  : string;
    ANULADO  : string;
    ID_LASERFICHE  : string;
    FLG_ESCANEADO  : string;
    EXISTE_ARCHIVADOS  : string;
    FLG_WEB  : string;
    FLG_VV  : string;
    IDDOCUMENTOSIGED  : string;
    FLG_MGD  : string;
    ID_MGD_ANEXOS  : string;
     
}

export class IExpedienteResponse {
  Post_ExpedienteResult: IExpediente;
}


export class IMovimiento{
      CODIGO_CORRELATIVO  : string;
      OFICINA_REMITENTE  : string;
      OFICINA_DERIVADA  : string;
      OBSERVACION  : string;
      ID_DOCUMENTO_ADJUNTO  : string;
      ACCION  : string;
      FECHA_DERIVACION  : string;
      FECHA_RECEPCION_FISICA  : string;
      PERSONAL  : string;
      CUENTA  : string;
      ESTADO  : string;
      ESTADO_DOC  : string;
      ID_DE  : string;
      EXISTE_ARCHIVADOS  : string;
      EXISTE_ADJUNTOS  : string;
      PRIORIDAD  : string;
      ID_CORRELATIVO_MATRIZ  : string;  

    }

    export class IMovimientoResponse {
      Post_MovimientoResult: IMovimiento;
    }
 