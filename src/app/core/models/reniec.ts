export class IReniec {
  ApellidoMaterno: string;
  ApellidoPaterno: string;
  Direccion: string;
  Dni: string;
  ErrMsg: string;
  EstadoCivil: string;
  Foto: any;
  Nombres: string;
  OK: boolean;
  Restriccion: string;
  Ubigeo: string;
}

export class IReniecResponse {
  Post_ReniecResult: IReniec;
}
