import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import {from, Observable, throwError} from 'rxjs';
import {catchError, mergeMap, timeout} from 'rxjs/operators';
import {UtilsService} from '../services/utils.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(private utils: UtilsService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.utils.checkConnection()) {
      this.utils.presentBasicAlert(
        'Error de conexión',
        'Por favor verifique su conexión a internet, es necesario para que pueda continuar.'
      );
      return throwError({ status: 0, statusText: 'No internet connection' });
    }
    return next.handle(request).pipe(
      timeout(10000),
      catchError((error: HttpErrorResponse) => {
        this.utils.presentBasicAlert(
          'Ocurrió un error',
          'Por favor inténtelo mas tarde.'
        );
        return throwError(error);
      }));
  }


}
